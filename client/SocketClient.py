import wx
import socket
from pubsub import pub
from threading import Thread
import pickle

class SocketClient(Thread):
    def __init__(self, host, port):
        Thread.__init__(self)
        self.setDaemon(True)
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn.connect((host, port))
        self.serviceKeyHandlers = {}
        self.registerServiceKeyHandler('auth_result', self.authResult)
        self.closed = False
        pub.subscribe(self.auth, 'auth')
        pub.subscribe(self.onDisconnect, 'disconnect')

    def run(self):
        while True:
            try:
                bufsize = self.conn.recv(16)
                if bufsize:
                    bufsize = int(bufsize)
                    # print('await len', bufsize)

                    data = self.conn.recv(0)

                    while bufsize > len(data):
                        if (bufsize - len(data)) > 1024:
                            b = 1024
                        else:
                            b = bufsize - len(data)

                        data += self.conn.recv(b)

                    # print('received len', len(data))
                    if data:
                        key, data = self.deserialize_data(data)
                        if not self.handleSericeKey(key, data):
                            wx.CallAfter(pub.sendMessage, 'socket_'+key, data=data)
                else:
                    break
            except socket.error as err:
                # print(err)
                break
        # print('disconect')

        if not self.closed:
            wx.CallAfter(pub.sendMessage, 'disconnect')
            # self.close()

    def auth(self, creds):
        self.emit('auth', creds)

    def authResult(self, data):
        if data['success']:
            wx.CallAfter(pub.sendMessage, 'auth_success', data=data)
        else:
            wx.CallAfter(pub.sendMessage, 'auth_fail', data=data)

    def handleSericeKey(self, key, data):
        if key in self.serviceKeyHandlers:
            self.serviceKeyHandlers[key](data)
            return True
        return False

    def registerServiceKeyHandler(self, key, handler):
        self.serviceKeyHandlers[key] = handler

    def emit(self, event, data):
        data = (event, data)
        data = self.serialize_data(data)

        bufsize = '%16s' %len(data)

        self.conn.send(bufsize.encode())
        self.conn.send(data)

    def close(self):
        self.closed = True
        self.conn.close()
        pub.unsubAll(listenerFilter=self.listFilt)

    def listFilt(self, listener):
        return listener.typeName().split('.')[0] == self.__class__.__name__

    def deserialize_data(self, data):
        return pickle.loads(data)

    def serialize_data(self, data):
        return pickle.dumps(data)

    # def __del__(self):
        # print('del socket client')

    def onDisconnect(self):
        self.close()