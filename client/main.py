import socket
from sys import exit
import wx
from client.SocketClient import SocketClient
from client.LoginDialog import LoginDialog
from pubsub import pub
from client.MainPanel import MainPanel


class MainFrame(wx.Frame):
    """"""

    def __init__(self):
        """Constructor"""
        wx.Frame.__init__(self, None, title="ImageTagger")
        self.socket = None
        self.Bind(wx.EVT_CLOSE, self.onExit)
        self.exited = False
        # Ask user to login

        init_data = self.showLoginForm()
        self.panel = MainPanel(self, init_data)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.panel, 1, wx.EXPAND)
        self.SetSizer(self.sizer)

        self.SetMinSize((700, 700))
        self.Show()
        self.Maximize(True)

    def showLoginForm(self):
        login_dialog = LoginDialog(self)
        code = login_dialog.ShowModal()
        login_dialog.Close()
        login_dialog.Destroy()

        if code != wx.ID_OK:
            exit()

        return login_dialog.init_data

    def emit(self, event, data=None):
        print(event, data)
        self.socket.emit(event, data)

    def openConnection(self, host, port):
        if self.socket:
            del self.socket
            self.socket = None
        try:
            self.socket = SocketClient(host, port)
        except socket.error:
            return False
        self.socket.start()
        pub.subscribe(self.emit, 'socket_emit')
        pub.subscribe(self.onDisconect, 'disconnect')
        return True

    def closeAndShowForm(self):
        self.Hide()
        self.exited = True
        init_data = self.showLoginForm()
        self.panel.Destroy()

        self.panel = MainPanel(self, init_data)
        self.sizer.Add(self.panel, 1, wx.EXPAND)
        self.Layout()
        self.exited = False
        self.Show()

    def onDisconect(self):
        if not self.exited:
            self.closeAndShowForm()

    def onExit(self, event):
        self.exited = True
        pub.sendMessage('disconnect')
        self.closeAndShowForm()
        # if self.socket:
        #     self.socket.close()
        # event.Skip()


if __name__ == "__main__":
    app = wx.App(False)
    frame = MainFrame()
    app.MainLoop()
