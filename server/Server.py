import argparse
import json
import os
import socket
import csv

import bcrypt

from server.ClientThread import ClientThread


class Server:
    def __init__(self, host, port, src_folder, labels_file, classes_file):
        self.classes = self.loadClasses(classes_file)
        self.config = self.loadConfig('config.json')
        self.images = self.loadImages(src_folder,self.config['allowedImageTypes'])
        self.image_labels, self.skipedImages = self.loadLabels(labels_file)

        print(self.image_labels)
        print(self.skipedImages)

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((host, port))
        self.socket.listen(self.config['maxUsers'])


        with open('users.json') as users_file:
            self.users = json.load(users_file)
        print('Server started at '+host+':'+str(port))

    def start(self):
        while True:
            conn, addr = self.socket.accept()
            print("Connection from: " + str(addr))
            ClientThread(self, conn).start()

    def auth(self, username, password):
        return username in self.users and bcrypt.checkpw(password.encode(), self.users[username].encode())

    def getClasses(self):
        return self.classes

    def loadClasses(self, path):
        classes = []
        if os.path.isfile(path):
            with open(path, 'r', encoding='utf8') as f:
                classes = json.load(f)
        return classes

    def loadLabels(self, path):
        labels = {}
        skiped = []
        with open(path,'r') as f:
            labels_reader = csv.DictReader(f, delimiter=',')
            for label in labels_reader:
                label = dict(label)
                if not label['cls']:
                    skiped.append(label['name'])
                labels[label.pop('name')] = label
        return labels, skiped

    def appendLabel(self, image, label, user):
        self.image_labels[image] = {'user': user, 'cls': label}

        if not label:
            self.skipedImages.append(image)
        self.saveCSV()

    def returnImage(self, image):
        self.images.append(image)

    def saveCSV(self):
        field_names = ['name', 'user', 'cls']
        with open('labels.csv','w+') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=field_names)

            writer.writeheader()
            for key, val in self.image_labels.items():
                writer.writerow({'name': key, **val})


    def getConfig(self, key=None):
        if key:
            if key in self.config:
                return self.config[key]
            else:
                return False
        return self.config

    def nextImage(self, skiped):
        if skiped:
            if len(self.skipedImages) > 0:
                return self.skipedImages.pop()
        else:
            if len(self.images) > 0:
                return self.images.pop()
        return None

    @staticmethod
    def defaultConfig():
        return {
            'salt': bcrypt.gensalt(),
            'authAttempts': 3,
            'maxUsers': 10,
            'allowedImageTypes': ['jpeg', 'jpg', 'tif'],
            'historySize': 5
        }

    def loadConfig(self, path):
        config = self.defaultConfig()
        if os.path.isfile(path):
            with open(path, 'r') as f:
                config = {**config, **json.load(f)}
        return config

    def loadImages(self, path, allowedTypes):
        allowedTypes = tuple(allowedTypes)
        images = []
        for address, dirs, files in os.walk('images'):
            for f in files:
                if f.lower().endswith(allowedTypes):
                    images.append(os.path.join(address, f))
        return images


def add_user(name, password):
    with open('users.json', 'r') as users_file:
        try:
            users = json.load(users_file)
        except:
            users = dict()
    with open('users.json', 'w+') as users_file:
        users[name] = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()
        json.dump(users, users_file)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--adduser', nargs=2, metavar=('username', 'password'))

    args = parser.parse_known_args()[0]

    if args.adduser:
        print('add user...', args.adduser)
        add_user(*args.adduser)
    else:
        parser.add_argument('--src_folder', type=str, required=True)
        parser.add_argument('--labels_file', type=str, required=True)
        parser.add_argument('--classes_file', type=str, required=True)
        parser.add_argument('--host', default='0.0.0.0')
        parser.add_argument('--port', type=int, default=5000)
        args = parser.parse_args()
        args = vars(args)
        args.pop('adduser', None)
        Server(**args).start()
